footer: PHP South Africa - September - 2016 | Introduction to Elasticsearch | Grant McMullin | @thatgrantoke
autoscale: true
build-lists: true
slidenumbers: true

![](bands.jpg)

# Introduction to Elasticsearch
### Grant McMullin | PixelBaste

---

![](bands.jpg)


# AKA

---

![](bands.jpg)

> There's no search like an Elastic Search
-- Zander Janse Van Resburg (2016)

---

![](bands.jpg)

# About Me

---

![](bands.jpg)

# Who am I

* Designer - Frontend - Backend - Damage Control
* Making stuff for the internet since '99 baby!
* Worked at some companies
* Went solo 2011
* Speak at a couple of events / organise some things
* Really good at copying and pasting from StackOverflow...

---

![](bands.jpg)

# Contact

* Twitter | *@thatgrantoke*
* Skype | *thatgrantoke*
* Email | *grant@pixelbaste.com*

---

![](bands.jpg)

# Why do we even need a search?

^ How many using a search engine such as Elastic, Solr = Logging? / Search?

---

![](bands.jpg)

> Software development is 100% about solving problems.
-- John Sonmez, 2012

---
![](bands.jpg)

# [fit] Access to information

---

![](bands.jpg)

# [fit] _Access_ to information

---

![](altavista.png)
![](google.png)

---
![](bands.jpg)

# [fit] Saturation - Differentiation - Expectation

---

![](bands.jpg)

> Search is like a conversation, it’s a bidirectional process between the user and system. It can be very rich as a real human conversation.
-- Luca Longo, 2015

---

![](altavista.png)
![](google.png)

---

![](bands.jpg)

# Relevancy

---

![](bands.jpg)

# `</rant>`
### (He can't help it, its part of his name)

---

![](bands.jpg)

# [fit] What is Elasticsearch?

---

![](bands.jpg)

# Elastic
##(the artist formally known as ElasticSearch BV)

---

![](bands.jpg)

# Products

- Elasticsearch
- Kibana
- Logstash

- Some other stuff

---

![](bands.jpg)

# Elasticsearch
- Publicly Released in 2010 (In Production 0.4 - Feb 2010)
- *6 years of development*
- Latest Stable release: 2.4.1
- Preview Version: 5.0.0-beta1

---

![](bands.jpg)

# Elasticsearch

- Built on Apache Lucene
- Written in Java
- Cross Platform
- Open Source (Apache License)
- Schema-Free *
- HTTP REST API
- Can use as little or as much config as you want
- Buzzword here

---

![](bands.jpg)

# Elasticsearch
- Focus on Scalability
	- Distributed
	- Can scale to as many nodes as required
- Data Safety
	- Clusters will reorganise and rebalance when nodes fail
	- Persists on change / Transaction logs
- *Real-time* data

---

![](bands.jpg)

## Notable use cases

---

![](bands.jpg)

- Adobe
- CERN
- Etsy
- Facebook
- GitHub
- Mozilla
- Netflix
- StumbleUpon
- Stack Exchange
- Wikimedia
- *More popular than Solr*

---

![](bands.jpg)

# The Basics

---

![](bands.jpg)

# Install

- Requires Java 7
- Now you can use `apt` or `yum` repos
- Download / extract

---

![](bands.jpg)

# Fire it up

`$ bin/elasticsearch`

# Run in background / daemon

`$ bin/elasticsearch -d`

---

![](bands.jpg)

# Check its running

`curl -X GET http://localhost:9200/`

```json
{
  "name" : "G-Force",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "wjaXvpwYRqKGZ5dzbgiVRg",
  "version" : {
    "number" : "2.4.1",
    "build_hash" : "c67dc32e24162035d18d6fe1e952c4cbcbe79d16",
    "build_timestamp" : "2016-09-27T18:57:55Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.2"
  },
  "tagline" : "You Know, for Search"
}
```


---

![](bands.jpg)

# Indexing / Creating

---

![](bands.jpg)

```json

{
	"name" : "Grant McMullin",
	"tel" : "+27234567890",
	"email" : "grant@pixelbaste.com",
	"twitter" : "@thatgrantoke",
	"description" : "The dude in the front",
	"dob" : "1986-04-16",
	"talks_given" : 9,
	"location": {
		"lat" : -26.07,
		"lon" : 28.024
	}
}

```
---

![](bands.jpg)

```bash
curl -X PUT http://localhost:9200/phpsa/speakers/1 -d '{
	"name" : "Grant McMullin",
	"tel" : "+27234567890",
	"email" : "grant@pixelbaste.com",
	"twitter" : "@thatgrantoke",
	"description" : "The dude in the front",
	"dob" : "1986-04-16",
	"talks_given" : 9,
	"location": {
		"lat" : -26.07,
		"lon" : 28.024
	}
}'
```

---

![](bands.jpg)

```json
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "1",
    "_version": 1,
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "created": true
}
```

---

![](bands.jpg)

# Reading

---

![](bands.jpg)

`curl -X GET http://localhost:9200/phpsa/speakers/1`

```json

{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "1",
    "_version": 1,
    "found": true,
    "_source": {
        "name": "Grant McMullin",
        "tel": "+27234567890",
        "email": "grant@pixelbaste.com",
        "twitter": "@thatgrantoke",
        "description": "The dude in the front",
        "dob": "1986-04-16",
        "talks_given": 9,
        "location": {
            "lat": -26.07,
            "lon": 28.024
        }
    }
}

```

^ Use HEAD for exists

---

![](bands.jpg)

# Updating

---

![](bands.jpg)

# On `PUT`

```json
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "1",
    "_version": 2,
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "created": false
}
```
---

![](bands.jpg)

## Deletes old record and puts new record, incrementing version

### `PATCH` will just return like `GET`

---

![](bands.jpg)

```bash
 curl -X POST http://localhost:9200/phpsa/speakers/1/_update -d '{
    "doc" : {
        "description" : "The dude still in the front"
    }
}'
```

^ doc or script runs locally on node - lower overhead

---

![](bands.jpg)

# Deleting

`curl -X DELETE http://localhost:9200/phpsa/speakers/2`

```json
{
    "found": true,
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "2",
    "_version": 2,
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    }
}
```

---

![](bands.jpg)

# Primary Data Storage

- Replicate current datastore into ES
- Use ES as primary datastore *- Braver than I am...*

^ Versions / Locking

---

![](bands.jpg)

# Bulk API

---

![](bands.jpg)

# On to the search!
## Finally

---

![](bands.jpg)

# Empty Search 
## GET ALL THE THINGS

```bash

curl -X GET http://localhost:9200/_search

```
---

![](bands.jpg)

```json

{
    "took": 42,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 1.0,
        "hits": [
            ...
        ]
    }
}

```

---

![](bands.jpg)

```json
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "1",
    "_score": 1.0,
    "_source": {
        "name": "Grant McMullin",
        "tel": "+27234567890",
        "email": "grant@pixelbaste.com",
        "twitter": "@thatgrantoke",
        "description": "The dude still in the front",
        "dob": "1986-04-16",
        "talks_given": 9,
        "location": {
            "lat": -26.07,
            "lon": 28.024
        }
    }
}
```

---

![](bands.jpg)

# Can search multiple types / indices
## Mix and match

```bash

curl -X GET http://localhost:9200/phpsa/_search

```

```bash

curl -X GET http://localhost:9200/_all/speakers/_search

```

---

![](bands.jpg)

# Search *Lite*
## Quick and Dirty

---

![](bands.jpg)

# Search *Lite*

```bash
curl -X GET http://localhost:9200/_all/speakers/_search\?q\=name:zander
```

```bash
curl -X GET http://localhost:9200/_all/speakers/_search\?q\=zander
```

---

![](bands.jpg)

```json
{
    "took": 50,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.15342641,
        "hits": [
            ...
        ]
    }
}
```

---

![](bands.jpg)

```json
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "3",
    "_score": 0.15342641,
    "_source": {
        "name": "Zander Janse Van Rensburg",
        "tel": "+27234567890",
        "email": "zander@geevcookie.com",
        "twitter": "@geevcookie",
        "description": "The Vaping oke",
        "dob": "1987-11-04",
        "talks_given": 12,
        "location": {
            "lat": -26.07,
            "lon": 28.024
        }
    }
}
```

---

![](bands.jpg)

- `+name:(grant zander)`
- `+date:>2016-09-01`
- `+(vaping front)`

<br/>


*Need to be encoded into url params*

---

![](bands.jpg)

```bash
curl -X GET http://localhost:9200/_all/speakers/_search\?q\=zander
```

---

![](bands.jpg)

# _all

---

![](bands.jpg)

```bash
curl -X GET http://localhost:9200/_all/speakers/_search\?q\=1986-04-16
```

```bash
curl -X GET http://localhost:9200/_all/speakers/_search\?q\=dob\:1986-04-16
```

---

![](bands.jpg)

```json
{
    "took": 6,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 0.11626227,
        "hits": [
            ...
        ]
    }
}
```

---

![](bands.jpg)

```json
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "1",
    "_score": 0.11626227,
    "_source": {
    	...
        "dob": "1986-04-16"
        ...
        }
    }
},
{
    "_index": "phpsa",
    "_type": "speakers",
    "_id": "3",
    "_score": 0.0047444105,
    "_source": {
    	...
        "dob": "1987-11-04",
        ...
        }
    }
}

```

---

![](bands.jpg)

```json
{
    "took": 27,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.30685282,
        "hits": [
            {
                "_index": "phpsa",
                "_type": "speakers",
                "_id": "1",
                "_score": 0.30685282,
                "_source": {
                    "name": "Grant McMullin",
                    "tel": "+27234567890",
                    "email": "grant@pixelbaste.com",
                    "twitter": "@thatgrantoke",
                    "description": "The dude still in the front",
                    "dob": "1986-04-16",
                    "talks_given": 9,
                    "location": {
                        "lat": -26.07,
                        "lon": 28.024
                    }
                }
            }
        ]
    }
}
```


---

![](bands.jpg)

# Mapping

---

![](bands.jpg)

# Automatic

```bash

curl -X GET http://localhost:9200/phpsa/speakers/_mapping

```

---

![](bands.jpg)

```json

{
    "phpsa": {
        "mappings": {
            "speakers": {
                "properties": {
                    "description": {
                        "type": "string"
                    },
                    "dob": {
                        "type": "date",
                        "format": "strict_date_optional_time||epoch_millis"
                    },
                    "email": {
                        "type": "string"
                    },
                    "location": {
                        "properties": {
                            "lat": {
                                "type": "double"
                            },
                            "lon": {
                                "type": "double"
                            }
                        }
                    },
                    "name": {
                        "type": "string"
                    },
                    "talks_given": {
                        "type": "long"
                    },
                    "tel": {
                        "type": "string"
                    },
                    "twitter": {
                        "type": "string"
                    }
                }
            }
        }
    }
}

```

^ Arrays ---- Naughty

---

![](bands.jpg)

# Exact

- Date
- Boolean
- Enumerator
- Numbers

- ES types: multi-field, ip, geo-point, geo-shape, etc.

---

![](bands.jpg)

> May is fun but June <br/>bores me.

---

![](bands.jpg)

# Full Text

- A search for *UK* should also return documents mentioning the *United Kingdom*.
- A search for *jump* should also match *jumped*, *jumps*, *jumping*, and perhaps even *leap*.
- *johnny walker* should match *Johnnie Walker*, and *johnnie depp* should match *Johnny Depp*.
- *fox news hunting* should return stories about *hunting* on *Fox News*, while *fox hunting news* should return news stories about *fox hunting*.

---

![](bands.jpg)

# Inverted Index / Analyzers

---

![](bands.jpg)

- Standard analyzer
	- splits word boundries / lowercse / removes punctuation
- Simple analyzer
	- splits on any whitespace / puctuation etc / lowercases
- Whitespace analyzers
	- splits on white space

---

![](bands.jpg)

# Language analyzers

- Stop Words
- Stem words
- Available: arabic, armenian, basque, brazilian, bulgarian, catalan, cjk, czech, danish, dutch, english, finnish, french, galician, german, greek, hindi, hungarian, indonesian, irish, italian, latvian, lithuanian, norwegian, persian, portuguese, romanian, russian, sorani, spanish, swedish, turkish, thai.


---

![](bands.jpg)

```bash

curl -X GET http://localhost:9200/_all/speakers/_search\?q\=description:vape

```

---

![](bands.jpg)


```json

{
    "took": 2,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 0,
        "max_score": null,
        "hits": []
    }
}

```

---

![](bands.jpg)

# Updating Mappings

---

![](bands.jpg)

 ```bash
 curl -X DELETE http://localhost:9200/phpsa
```
```json
{"acknowledged":true}
```

---

![](bands.jpg)

```bash
curl -X PUT http://localhost:9200/phpsa/ -d '{
  "mappings": {
    "speakers" : {
      "properties" : {
        "description" : {
          "type" :    "string",
          "analyzer": "english"
        }
      }
    }
  }
}'
```

```json
{"acknowledged":true}
```

---

![](bands.jpg)

```bash

curl -X GET http://localhost:9200/_all/speakers/_search\?q\=description:vape

```

---

![](bands.jpg)

```json

{
    "took": 2,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.19178301,
        "hits": [
            {
                "_index": "phpsa",
                "_type": "speakers",
                "_id": "3",
                "_score": 0.19178301,
                "_source": {
                	...
                    "description": "The Vaping oke",
                    ...
                }
            }
        ]
    }
}

```

---

![](bands.jpg)

# Full Body Search

```bash

curl -X GET http://localhost:9200/_search
{
    "query": {
        "match_all": {}
    }
}

```

---

![](bands.jpg)

> `GET` request with a body? `GET`TFO

---

![](bands.jpg)

# QueryDSL

---

![](bands.jpg)

```json

{
    QUERY_NAME: {
        ARGUMENT: VALUE,
        ARGUMENT: VALUE,...
    }
}

```

---

![](bands.jpg)

```json

{
    QUERY_NAME: {
        FIELD_NAME: {
            ARGUMENT: VALUE,
            ARGUMENT: VALUE,...
        }
    }
}

```

---

![](bands.jpg)

```json
{
    "query": {
        "match": {
            "description": "vape"
        }
    }
}
```
---

![](bands.jpg)

```json
{
    "bool": {
        "must": {
            "match": {
                "description": "vape"
            }
        },
        "should": [
            {
                "range": {
	                "dob": {
	                    "gt": "1998-01-01"
	                }
	            }
            }
        ],
        "must_not": {
            "match": {
                "name": "grant"
            }
        },
        "filter": {
            "range": {
                "talks": {
                    "gt": 10
                }
            }
        }
    }
}
```

---

![](bands.jpg)

```json

{
    "filter": {
        "range": {
            "talks": {
                "gt": 10
            }
        }
    }
}


```

---

![](bands.jpg)

# Filters vs Queries

> As a general rule, use query clauses for full-text search or for any condition that should affect the relevance score, and use filters for everything else

---

![](bands.jpg)

# Geolocation [^1]

[^1]: Assuming Earth is round, but is it? #FlatEarthSociety

---

![](bands.jpg)

# Pythagoras 

---

![](bands.jpg)

# Haversine

---

![](bands.jpg)

# Geo Distance

---

![](bands.jpg)

```json

{
    "phpsa": {
        "mappings": {
            "speakers": {
                "properties": {
                    "location": {
                        "properties": {
                            "lat": {
                                "type": "double"
                            },
                            "lon": {
                                "type": "double"
                            }
                        }
                    }
                }
            }
        }
    }
}

```
---

![](bands.jpg)

# Update Mappings

```json

{
    "phpsa": {
        "mappings": {
            "speakers": {
                "properties": {
                	...
                    "location": {
                        "type": "geopoint"
                    }
                   	...
                }
            }
        }
    }
}

```

---

![](bands.jpg)

# GeoDistance Query

```json
{
    "bool" : {
        "must" : {
            "match_all" : {}
        },
        "filter" : {
            "geo_distance" : {
                "distance" : "20km",
                "location" : {
                    "lat" : -26.00,
                    "lon" : 28.00
                }
            }
        }
    }
}
```

---

![](bands.jpg)

# Items not mentioned

- Pagination
- Sorting
- Aggregation

---

![](bands.jpg)

# Conclusion

---

![](bands.jpg)

# Contact

* Twitter | *@thatgrantoke*
* Skype | *thatgrantoke*
* Email | *grant@pixelbaste.com*

---
![](bands.jpg)

# The End

---

Background photo - Bill Ebbesen 
https://commons.wikimedia.org/wiki/File:Rubber_bands_-_Colors_-_Studio_photo_2011.jpg
Creative Commons Attribution-Share Alike 3.0 Unported 