# Aint not Search Like an Elastic Search
## Grant McMullin
### Friday 30 September 2016 at 10:15

> During this talk Grant will introduce us to what is most likely the coolest way to search. He will be covering what ElasticSearch is, how you can use it, and why it is so powerful!

- [Joind.in](https://joind.in/talk/c2bb9)
- [PHP South Africa](http://phpsouthafrica.com/)

# Contact

* Twitter | *@thatgrantoke*
* Skype | *thatgrantoke*
* Email | *grant@pixelbaste.com*